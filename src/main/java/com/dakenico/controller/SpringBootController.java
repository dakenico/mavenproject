package com.dakenico.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBootController {
   @RequestMapping("/")
   public String sayHello() {
      return "Hello Spring Boot!!";
   }
}
